﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Unfriend_Finder
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        //Cannot declare instance member in a static class. That's why "static" keyword has been added:
        //http://documentation.devexpress.com/#CodeRush/CustomDocument10212
        //Some changes were made in Program.cs. Before these changes I tried to do this: 
        // public static Form1 instance = new Form1(); Because an exception was thrown:
        //SetCompatibleTextRenderingDefault must be called before the first IWin32Window object is created in the application
        //This article explains how to solve this issue
        //http://goo.gl/E0xYt5
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(f1); //instead of this line substituted with below
            Application.Run(Form1.Instance);
        }
    }
}
