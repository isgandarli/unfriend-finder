﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Facebook;
//Detailed information  about Clipboard Monitoring:
// http://www.radsoftware.com.au/articles/clipboardmonitor.aspx
namespace Unfriend_Finder
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            nextClipboardViewer = (IntPtr)SetClipboardViewer((int) this.Handle); //HandleRef - Wraps a managed object holding a handle to a 
            //resource that is passed to unmanaged code using platform invoke.
            //Handle - Gets the handle to a resource.
            this.CreateMyBorderlessWindow();
            //base.ShowInTaskbar = false;   Warning: This line causes problem, if you commented out problem for some reason Clipboard Monitor
            //works only one time
            this.TopMost = true;
            button1.Text = @"Simply copy access token from browser and if you copy valid access token this form will be disappeared
Note:Don't forget switch Graph API Explorer to Unfriend Finder";
            //SetForegroundWindow(this.Handle);   if base.TopMost = true; won't work use this
        }
        public void CreateMyBorderlessWindow()
        {
            //base.FormBorderStyle = FormBorderStyle.None;
            base.StartPosition = FormStartPosition.CenterScreen;
            base.ControlBox = false;
            this.BackColor = Color.AliceBlue;
        }
        //[System.Runtime.InteropServices.DllImport("user32.dll")]
        //public static extern bool SetForegroundWindow(IntPtr hWnd);       if base.TopMost = true; won't work use this
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SetClipboardViewer(int hWndNewViewer);
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool ChangeClipboardChain(IntPtr hWndRemove,
                              IntPtr hWndNewNext);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr hwnd, int wMsg,
                                       IntPtr wParam,
                                       IntPtr lParam);
        IntPtr nextClipboardViewer;
        protected override void
          WndProc(ref System.Windows.Forms.Message m)
        {
            // defined in winuser.h
            const int WM_DRAWCLIPBOARD = 0x308; //776 
            const int WM_CHANGECBCHAIN = 0x030D; //778
            switch (m.Msg)
            {
                case WM_DRAWCLIPBOARD:
                    DisplayClipboardData();
                    SendMessage(nextClipboardViewer, m.Msg, m.WParam,
                                m.LParam);
                    break;
                case WM_CHANGECBCHAIN:
                    if (m.WParam == nextClipboardViewer)
                        nextClipboardViewer = m.LParam;
                    else
                        SendMessage(nextClipboardViewer, m.Msg, m.WParam,
                                  m.LParam);
                    break;
                default: base.WndProc(ref m); //When overriding WndProc I must pass any unhandled messages to the base class by calling base.WndProc
                    break;
            }
        }
        void DisplayClipboardData()
        {
            string str;
            try
            {
                IDataObject idata = new DataObject();
                idata = Clipboard.GetDataObject();
                if (idata.GetDataPresent(DataFormats.Rtf) || idata.GetDataPresent(DataFormats.Text))
                {
                    //(string)idata.GetData(DataFormats.Text);
                    str = (string)idata.GetData(DataFormats.Text);
                    if (str != "" && str != null)
                    {
                        str = Form1.RenewToken(str);
                        if (str != "" && str != null)
                        {
                            button1.Text += "Success";
                            Form1.token = str;
                            ChangeClipboardChain(this.Handle, nextClipboardViewer);
                            this.Close();
                        }
                        else throw new FacebookOAuthException();
                    }
                }
            }
            catch (FacebookOAuthException)
            {
                button1.Text += "\n" + "invalid access token";
            }
            catch (Exception e)
            {
                MessageBox.Show("form2" + e.Message);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //Form1.token = (string)Clipboard.GetData(DataFormats.StringFormat);
            //this.Close();
            //MessageBox.Show("--" + Form1.token + "--");
            //this.Close();
        }
    }
}
