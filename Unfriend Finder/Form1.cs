﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Facebook;
using Facebook.Reflection;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Timers;
using System.Diagnostics;
using System.Threading;
using System.Web; //without adding reference this line will throw compile time error. You have to browse System.Web.Extensions.dll manually
// from destination. Further information: http://goo.gl/FyVBLx
namespace Unfriend_Finder
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}
		public static /*readonly*/ Form1 instance = new Form1();
		public static Form1 Instance
		{
			get
			{
				return instance;
			}
		}
		public static string token
		{
			get;
			set;
		}
		public static string path
		{
			get;
			set;
		}
		public static string _old
		{
			get;
			set;
		}
		public static bool b
		{
			get;
			set;
		}
		dynamic myInfo;
		Dictionary<string, string> friend_list = new Dictionary<string, string>();
		Dictionary<string, string> old_friend_list = new Dictionary<string, string>();
		Dictionary<string, string> unfriend_list = new Dictionary<string, string>();
		Dictionary<string, string> new_friend_list = new Dictionary<string, string>();
		public void Main_Part()
		{
			var fb = new FacebookClient();
			var timer_renew = new System.Timers.Timer(500000);
			timer_renew.Elapsed += new System.Timers.ElapsedEventHandler(RenewTokenEvent);
			try
			{
				using (StreamReader infile = new StreamReader(@"accesstokenfile.txt"))
				{
					token = infile.ReadToEnd();
				}
				if (token != "" && token != null)
				{
					fb = new FacebookClient(token);
					myInfo = fb.Get("/me");
				}
			}
			catch (FileNotFoundException) 
			{
				GetToken();
			}
			catch (FacebookOAuthException)
			{
				GetToken();
			}
			catch (Exception e)
			{
				MessageBox.Show("Unexpected error occured. You have to take appropriate actions to solve this problem\n" + e + " " + e.Message);
				//send feedback to my e-mail
			}
			fb = new FacebookClient(token);
			myInfo = fb.Get("/me/friends");
			Current_friends(myInfo);
			timer_renew.Enabled = true;
		}
		public static string RenewToken(string existingToken)
		{
			var fb = new FacebookClient();
			dynamic result = fb.Get("oauth/access_token",
									new
									{
										client_id = "134586943416536",
										client_secret = "6fdd2fd18a1129c81243c02b4ec5d63b",
										grant_type = "fb_exchange_token",
										fb_exchange_token = existingToken
									});
			return result.access_token;//client_credentials   
		}
		private static void RenewTokenEvent(object source, ElapsedEventArgs e)
		{
			string test = RenewToken(token);
			using (StreamWriter accesstokenfile = new StreamWriter(@"accesstokenfile.txt"))
			{
				accesstokenfile.Write(test);
			}
		}
		private static void GetToken()
		{
			//            
			Process.Start("https://developers.facebook.com/tools/explorer?method=GET");
				Form2 f2 = new Form2();
				try
				{
					f2.ShowDialog();  //If valid access token is on the clipboard, processes in the form2 will complete before f2.ShowDialog();
				}
				catch (ObjectDisposedException)
				{
				}
				using (StreamWriter accesstokenfile = new StreamWriter(@"accesstokenfile.txt"))
			{
				accesstokenfile.Write(token);
			}
		}
		public class FacebookFriend
		{
			public string id { get; set; }
			public string name { get; set; }
		}
		public class Friends
		{
			public List<FacebookFriend> data { get; set; }
		}
		static void Current_friends(dynamic stuff)
		{
			using (StreamWriter outfile = new StreamWriter(@"newfriendlist.txt"))
			{
				outfile.Write(stuff);
			}
			foreach (var info in stuff.data)
			{
				instance.friend_list.Add(info.id, info.name);
			}
			instance.listBox1.DataSource = new BindingSource(instance.friend_list, null);
			instance.listBox1.DisplayMember = "Value";
			instance.label1.Text += ": " + instance.listBox1.Items.Count;
			Check_unfriend();
		}
		static void Check_unfriend()
		{
			try
			{
				using (StreamReader infile = new StreamReader(@"oldfriendlist.txt"))
				{
						_old = infile.ReadToEnd();    
				}
				Friends old = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<Friends>(_old); // http://goo.gl/lT8xW 
				//Deserializing json data returned from Facebook into a dynamic type is not easy as it seems. It's impossible to deserialize json
				// of friend list to a dictionary, coz its structure is different, there's no id and name properties. 
				// You have to add dll for working JavaScriptSerializer() method. Look at the commentsabove where System.Web added http://goo.gl/FyVBLx
				foreach (var old_info in old.data)
				{
					instance.old_friend_list.Add(old_info.id, old_info.name);
				}
				b = false;
				foreach (KeyValuePair<string, string> entry in instance.old_friend_list /*instance.old_friend_list*/)
				{
					if (!instance.friend_list.ContainsKey(entry.Key))
					{
						instance.unfriend_list.Add(entry.Key, entry.Value);
						b = true;
					}
				}
				if (b)
				{
					instance.listBox2.DataSource = new BindingSource(instance.unfriend_list, null);
					instance.listBox2.DisplayMember = "Value";
					instance.label2.Text += ": " + instance.listBox2.Items.Count;
				}
				else
				{
					instance.listBox2.Items.Add("No Unfriend detected");
					instance.label2.Text += ": " + "0";
				}
				using (StreamReader unfrnd = new StreamReader(@"unfriended.txt"))
					{
						_old = unfrnd.ReadToEnd();
					}
					Friends unfriendlist_Friends = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<Friends>(_old);
					foreach (var unfriend_info in unfriendlist_Friends.data)
					{
						foreach (KeyValuePair<string, string> ob in instance.unfriend_list)
						{
							if (unfriend_info.id != ob.Key)
							{
								instance.unfriend_list.Add(ob.Key, ob.Value);
								break;
							}
						}
					}
					if (instance.Show_all_unfriends.Checked == true)
					{
						instance.listBox2.Items.Clear();
						instance.listBox2.DataSource = new BindingSource(instance.unfriend_list, null);
						instance.listBox2.DisplayMember = "Value";
						instance.label2.Text += " till now : " + instance.listBox2.Items.Count;
					}
					if (b)
					{
						using (StreamWriter unfriended = new StreamWriter(@"unfriended.txt"))
						{
							unfriended.Write(instance.unfriend_list.ToString());
						}
					}
			}
			catch (FileNotFoundException)
			{
				instance.listBox2.Items.Add(@"Cannot show unfriend list. Either you are using this program for the first time or some data has been deleted. 
For seeing effect try to check friend list again");
			}
			Check_New_Friends();
		}
		static void Check_New_Friends()
		{
			b = false;
			foreach (KeyValuePair<string, string> entry in instance.friend_list)
			{
				if (!instance.old_friend_list.ContainsKey(entry.Key))
				{
					instance.new_friend_list.Add(entry.Key, entry.Value);
					b = true;
				}
			}
			if (b)
			{
				instance.listBox3.DataSource = new BindingSource(instance.new_friend_list, null);
				instance.listBox3.DisplayMember = "Value";
				instance.label3.Text += ": " + instance.listBox3.Items.Count;
			}
			else
			{
				instance.listBox3.Items.Add("No Newfriends detected");
				instance.label3.Text += ": " + "0";
			}
			try
			{
				using (StreamReader nwfrnd = new StreamReader(@"all_new_friendeds.txt"))
				{
					_old = nwfrnd.ReadToEnd();
				}
				if (_old != "")
				{
					Friends newfriendlist_Friends = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<Friends>(_old);
					foreach (var newfriend_info in newfriendlist_Friends.data)
					{
						foreach (KeyValuePair<string, string> ob in instance.new_friend_list)
						{
							if (newfriend_info.id != ob.Key)
							{
								instance.new_friend_list.Add(ob.Key, ob.Value);
								break;
							}
						}
					}
					if (instance.Show_all_newfriends.Checked == true)
					{
						instance.listBox3.Items.Clear();
						instance.listBox3.DataSource = new BindingSource(instance.new_friend_list, null);
						instance.listBox3.DisplayMember = "Value";
						instance.label3.Text += " till now : " + instance.listBox3.Items.Count;
					}
					if (b)
					{
						using (StreamWriter unfriended = new StreamWriter(@"all_new_friendeds.txt"))
						{
							unfriended.Write(instance.new_friend_list.ToString());
						}
					}
				}
			}
			catch (FileNotFoundException)
			{
				MessageBox.Show("Cannot show all, close program and try again", "Sorry!");
			}
			File.Delete(@"C:\Users\Mahammad\Documents\Visual Studio 2012\Projects\Unfriend Finder\Unfriend Finder\bin\Debug\oldfriendlist.txt");
			System.IO.File.Move(@"C:\Users\Mahammad\Documents\Visual Studio 2012\Projects\Unfriend Finder\Unfriend Finder\bin\Debug\newfriendlist.txt",
				@"C:\Users\Mahammad\Documents\Visual Studio 2012\Projects\Unfriend Finder\Unfriend Finder\bin\Debug\oldfriendlist.txt");
		}
		public void button2_Click(object sender, EventArgs e)
		{
		}
		public void button1_Click(object sender, EventArgs e)
		{
				Main_Part();
				button1.Text = "Check Now!";
		}
		private void Form1_Load(object sender, EventArgs e)
		{
		}
		private void checkBox1_CheckedChanged(object sender, EventArgs e)
		{
			if (instance.Info.Checked == true)
			{
				MessageBox.Show(@"To see your unfriend and new friends list push Start button. Check Show all till now before pushing start button.
Note: Some features has not been added yet. Set Interval, Autostarta and Check Now! are only design elements, that is they are not functional yet.
", "Information");
			}
		}
	}
}
